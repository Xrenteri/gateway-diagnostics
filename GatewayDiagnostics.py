import socket
import time
import datetime

def main():

    #gets timestamp
    ts = time.time()
    #formats timestamp
    ts = datetime.datetime.now().strftime("%I:%M:%S %p")
    
    #setsup heartbeat string
    heartbeat = "Heartbeat... Gateway ID: 0001, Timestamp:" + ts
    
    #sets up socket
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    #binds socket to local machine's IP address, and sets port to port number below
    soc.bind((socket.gethostname(), 6191))
    
    #lets socket listen to connections, and sets how many connections allowed
    soc.listen(1)
    
    while True:
        #connects sockets
        clientsocket, address = soc.accept()
        print(f"Connection from {address} has been established!")
        
        #sends data to client socket
        clientsocket.send(bytes(heartbeat, "utf-8"))
        
    #closes socket
    soc.close()
    
main()